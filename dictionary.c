#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int MALLOC_SIZE = 50;

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	// TODO
	// Allocate memory for an array of strings (arr).
	int index = 0;
	char **arr = 0;
	// Read the dictionary line by line.
	char fileline[50];
	while(fgets(fileline, MALLOC_SIZE, in))
	{
		// Mallocs if first index, else reallocs for all others
		if(index == 0)
			arr = malloc(sizeof(char*));
		else
		{
			arr = realloc(arr, (index + 1) * sizeof(char*));
		}
		//Determine length of string read from file
		int stringlength = strlen(fileline);
		
		fileline[stringlength - 1] = '\0';
		
		// Allocate memory for the string (str).
		char *str = malloc(stringlength * sizeof(char));
		
		// Copy each line into the string (use strcpy).
		strcpy(str, fileline);
		
		// Attach the string to the large array (assignment =).
		*(arr + index) = str;
		
		//Move to next entry
		index++;
	}
	fclose(in);
	
	// The size should be the number of entries in the array.
		*size = index;
		
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}